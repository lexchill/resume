import re

class Regex:
    regex_get_shared_date = re.compile(r'window._sharedData')
    regex_get_consumer_js = re.compile(r'.*Consumer.js.*', flags=re.DOTALL)
    regex_get_query_id_for_comments = re.compile(r'queryId:"\w*"', flags=re.DOTALL)