import requests
from user_agent import  generate_user_agent
from instagram.exceptions.except_requests import Response404
import json


class Requests:
    """Класс-обертка для запросов"""

    __session = requests.Session()
    __session.headers = {'user-agent': generate_user_agent(device_type="all", os=('mac', 'linux'))}

    @classmethod
    def __get_proxy(cls) -> dict:
        """Функуция для генерации прокси"""
        pass

    @classmethod
    def __request(cls, method: str, get_json: bool, **kwargs):
        """Функция для выполнения запроса"""
        if not kwargs.get('proxies', None):
            kwargs['proxies'] = cls.__get_proxy()
        response = cls.__session.request(method, **kwargs)
        if response.status_code == 400:
            raise Response404(f'{method} request with {kwargs} return status code = {response.status_code}')
        if get_json:
            return json.loads(response.text)
        else:
            return response.text


    @classmethod
    def request(cls, method: str, session: requests.Session = None, get_json: bool = None, **kwargs):
        if session:
            response = session.request(method=method, **kwargs)
            if response.status_code == 400:
                raise Response404(f'{method} request with {kwargs} return status code = {response.status_code}')
            else:
                if get_json:
                    return json.loads(response.text)
                else:
                    return response.text
        else:
            return cls.__request(method=method, get_json=get_json, **kwargs)

    @classmethod
    def get_session(cls) -> requests.Session:
        session = requests.Session()
        session.headers = {'user-agent': generate_user_agent(device_type="all", os=('mac', 'linux'))}
        return session

