from instagram.libs.requests import Requests
from instagram.libs.regex import Regex
from instagram.libs.db import Db
from instagram.exceptions.except_insta import *

from bs4 import BeautifulSoup

import json


class InstaBase:
    inst_url = 'https://www.instagram.com/'
    inst_api_url = 'https://instagram.com/graphql/query/'

    req = Requests()
    db = Db()

    @classmethod
    def __get_dict_auth(cls, login: str, passwd: str):
        return {
            'username': login,
            'enc_password': '', #secret))
            'queryParams': {},
            'optIntoOneTap': 'false'
        }


    @classmethod
    def _get_inst_session(cls, login: str, passwd: str):
        session = cls.req.get_session()
        session.headers.update({'Referer': cls.inst_url + 'accounts/login/'})
        response_text = cls.req.request(method='GET', session=session, url=cls.inst_url)
        html_content = BeautifulSoup(response_text, 'html.parser')
        body = html_content.find('body')

        java_script_tag = body.find("script", text=Regex.regex_get_shared_date)
        need_data_text = java_script_tag.get_text().replace('window._sharedData = ', '')[:-1]
        need_data_dict: dict = json.loads(need_data_text)

        if need_data_dict.get('config', None):
            csrf = need_data_dict['config'].get('csrf_token', None)
            if not csrf:
                raise NotFoundCSRFtoken('CSRF token not found')

            session.headers.update({'X-CSRFToken': csrf})


            auth_data = cls.__get_dict_auth(login=login, passwd=passwd)
            auth_answer = cls.req.request(method='POST',
                                          session=session,
                                          get_json=True,
                                          data=auth_data,
                                          url=cls.inst_url + 'accounts/login/ajax/',
                                          allow_redirects=True
                                          )

            if auth_answer.get('authenticated', None) is not True or auth_answer.get('status', None) != 'ok':
                raise AuthenticatedError(f'Authenticated error. login = {login}, password = {passwd}, csrf = {csrf}')

            return session
        else:
            raise NotFoundNeedKey(f'dict = {need_data_dict}\nkey = "config"')

    @classmethod
    def _get_account_info(cls, session, username: str):
        response_dict = cls.req.request(method='GET',
                                        session=session,
                                        get_json=True,
                                        url=cls.inst_url + username,
                                        params={'__a': 1}
                                        )
        return response_dict['graphql']['user']

    @classmethod
    def _registration_in_inst(cls):
        pass







