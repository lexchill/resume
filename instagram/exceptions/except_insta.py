class NotFoundCSRFtoken(Exception):
    pass

class AuthenticatedError(Exception):
    pass

class NotFoundNeedKey(Exception):
    pass

class NotExistAccountInstagram(Exception):
    pass