from peewee import Model, TextField, IntegerField
from instagram.configs.config_db import database, database_schema

class Comments(Model):
    id = IntegerField()
    text = TextField()

    class Meta:
        db_table = 'comments'
        database = database
        schema = database_schema
