from peewee import Model, TextField, IntegerField
from instagram.configs.config_db import database, database_schema

class User(Model):
    id = IntegerField()
    username = TextField()

    class Meta:
        db_table = 'user'
        database = database
        schema = database_schema
