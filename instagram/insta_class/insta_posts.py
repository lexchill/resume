from instagram.libs.insta_base import InstaBase
from instagram.exceptions.except_insta import *
from instagram.libs.regex import Regex
from bs4 import BeautifulSoup

class InstaPosts(InstaBase):

    def __init__(self, login: str, passwd: str):
        self.session = self._get_inst_session(login=login, passwd=passwd)

    def __get_query_hash_for_comments(self, short_code: str):
        response_text = self.req.request(
            method='GET',
            session=self.session,
            url=self.inst_url + 'p/' + short_code
        )

        html_content = BeautifulSoup(response_text, 'html.parser')
        java_script_tag = html_content.find('link',
                                  attrs={'type':'text/javascript'},
                                  href=Regex.regex_get_consumer_js)
        path_to_file_js = java_script_tag.get('href', None)
        if not path_to_file_js:
            raise NotFoundNeedKey(f'Not found key = "href" in js tag = {java_script_tag}')

        response_text = self.req.request(
            method='GET',
            session=self.session,
            url=self.inst_url[:-1] + path_to_file_js
        )

        query_hash = Regex.regex_get_query_id_for_comments.findall(response_text)[0].replace('queryId:"', '').replace('"', '')
        if query_hash:
            return query_hash
        else:
            return

    def get_account(self, username: str):
        account = self._get_account_info(session=self.session, username=username)
        if not account:
            raise NotExistAccountInstagram(f'Account instargam from username = {username} was not got')

        if not account.get('id', None):
            raise NotFoundNeedKey(f'Not found key = "id" in account = {account}')

        return account

    def get_posts(self, id_user: str, count_part_posts: int = 1):
        params_for_posts = {
            'query_id': '17888483320059182',
            'id': id_user,
            'first': 12,
            'after': None
        }
        while True:
            if count_part_posts == 0:
                break
            response_dict = self.req.request(
                method='GET',
                get_json=True,
                session=self.session,
                url=self.inst_api_url,
                params=params_for_posts
            )

            data = response_dict.get('data', None)
            if not data:
                raise NotFoundNeedKey(f'Not found key "data" in response_dict = {response_dict}')

            if data['user']['edge_owner_to_timeline_media']['page_info']['has_next_page']:
                params_for_posts['after'] = data['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
                yield data['user']['edge_owner_to_timeline_media']['edges']
                count_part_posts -= 1
            else:
                yield data['user']['edge_owner_to_timeline_media']['edges']
                break

    def get_comments(self, short_code: str, count_posts: int = None):
        params_for_comments = {
            'query_hash': self.__get_query_hash_for_comments(short_code=short_code),
            'shortcode': short_code,
            'first': 12,
            'after': ''
        }
        count = count_posts
        while True:
            if count == 0:
                break
            response_dict = self.req.request(
                method='GET',
                get_json=True,
                session=self.session,
                url=self.inst_api_url,
                params=params_for_comments
            )

            data = response_dict.get('data', None)
            if not data:
                raise NotFoundNeedKey(f'Not found key "data" in response_dict = {response_dict}')
            if data['shortcode_media']['edge_media_to_parent_comment']['page_info']['has_next_page']:
                params_for_comments['after'] = data['shortcode_media']['edge_media_to_parent_comment']['page_info']['end_cursor']
                count -= 1
                yield data['shortcode_media']['edge_media_to_parent_comment']['edges']
            else:
                yield data['shortcode_media']['edge_media_to_parent_comment']['edges']
                break




