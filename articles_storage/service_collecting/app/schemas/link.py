from pydantic import BaseModel, Field


class Link(BaseModel):
    url: str = Field(...,
                     regex=r"^(https?:\/\/)?([\w-]{1,32}\.[\w-]{1,32})[^\s@]*$",
                     description="Url for resource",
                     max_length=250)

    class Config:
        schema_extra = {
            "example": {
                "url": "https://habr.com/ru/post/269347/"
            }
        }
