from fastapi import APIRouter, Body, Depends

from ..schemas.link import Link
from ..libs.depends import auth
from service_collecting.app.controllers.celery_tasks import Task

create_task_router = APIRouter(
    prefix="/articles",
    tags=["create_task"]
)


@create_task_router.post("/save")
async def create(link: Link = Body(...), user_is_auth: bool = Depends(auth)):
    # Task.save_html.delay(link.url)
    # return 'ok'
    Task.save_article(link.url)
    return 'ok'
