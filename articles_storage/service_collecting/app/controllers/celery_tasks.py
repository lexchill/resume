from celery import Celery

from ..libs.web_factory import WebPageFactory
from ..libs.db import DataBase

celery_app = Celery('celery_tasks', broker='amqp://', backend='rpc://')


class Task:
    @staticmethod
    @celery_app.task
    def save_article(url: str):
        web_article = WebPageFactory.get_web_template(url=url)
        DataBase.save_article(**dict(web_article))
