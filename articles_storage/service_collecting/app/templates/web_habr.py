from . import WebPageTemplate, Tag


class HabrWebPage(WebPageTemplate):
    post_body_id: str = "post-content-body"

    def get_html_article(self) -> Tag:
        return self._body.find(attrs={"id": self.post_body_id})
