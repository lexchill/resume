from ..libs.requests import Request
from ..templates import WebPageTemplate
from ..templates.web_habr import HabrWebPage


class WebPageFactory:
    @staticmethod
    def get_web_template(url: str) -> WebPageTemplate:
        text_page = Request.request(method='GET', url=url)

        if "habr" in url:
            return HabrWebPage(url=url, html5_text=text_page)
