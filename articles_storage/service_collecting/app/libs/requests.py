import json
import requests

from json import JSONDecodeError
from user_agent import generate_user_agent
from requests.exceptions import ConnectionError, ReadTimeout, ProxyError


class Request:
    __session = requests.Session()
    __session.headers = {'user-agent': generate_user_agent(device_type="all", os=('mac', 'linux'))}

    @classmethod
    def request(cls, method: str, json_res: bool = False, download_photo: bool = False,
                session: requests.Session = None, **kwargs):
        try:
            if kwargs.get('timeout', None) is None:
                kwargs['timeout'] = 10

            if session is None:
                response = cls.__session.request(method, **kwargs)
            else:
                response = session.request(method, **kwargs)

            if response.ok:
                if json_res:
                    return json.loads(response.text)
                else:
                    if download_photo:
                        return response.content
                    else:
                        return response.text

        except JSONDecodeError:
            pass
        except ProxyError:
            pass
        except ConnectionError:
            pass
        except ReadTimeout:
            pass
