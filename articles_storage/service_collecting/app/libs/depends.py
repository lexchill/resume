import jwt
from time import time
from fastapi import HTTPException
from fastapi.requests import Request
from fastapi.security.base import SecurityBase
from fastapi.openapi.models import HTTPBase as HTTPBaseModel
from fastapi.security.utils import get_authorization_scheme_param

from .config import app_config


class JWTAuth(SecurityBase):
    def __init__(self, auto_error: bool = True):
        self.scheme_name = "Bearer"
        self.model = HTTPBaseModel(scheme=self.scheme_name)
        self.auto_error = auto_error

    async def __call__(self, request: Request):
        try:
            header_authorization: str = request.headers.get("Authorization", None)
            scheme_access, access_token = get_authorization_scheme_param(header_authorization)
            if not scheme_access or scheme_access.lower() != self.scheme_name.lower():
                if self.auto_error:
                    raise HTTPException(status_code=401, detail="Not authenticated")
                else:
                    return None

            jwt_info = jwt.decode(access_token, app_config.jwt_public, algorithms=[app_config.jwt_algorithm],
                                  options={'verify_signature': True})

            if jwt_info.get("expires", time()) <= time():
                raise HTTPException(status_code=401, detail="Not authenticated")

        except (jwt.exceptions.InvalidSignatureError, jwt.exceptions.DecodeError):
            raise HTTPException(status_code=401, detail="Not authenticated")


auth = JWTAuth(auto_error=True)
