from ..models.articles import CollectionArticles
from ..models.articles_content import CollectionArticlesContent


class DataBase:
    @staticmethod
    def save_article(**kwargs) -> bool:
        content = kwargs.pop("content", "")
        id_article = CollectionArticles.insert_article(**kwargs)
        if id_article:
            CollectionArticlesContent.insert_article(**{"id_article": id_article, "content": content})
            return True
