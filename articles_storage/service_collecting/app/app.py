from fastapi import FastAPI

from .routes.collecting import create_task_router

app_collection = FastAPI()
app_collection.include_router(create_task_router)
