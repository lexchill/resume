import uvicorn

if __name__ == "__main__":
    uvicorn.run("app.app:app_collection", host="127.0.0.1", port=8082, reload=True)
