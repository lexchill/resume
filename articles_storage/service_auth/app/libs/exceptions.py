class InvalidDataBaseUrlException(Exception):
    pass


class NotFoundConfigDB(Exception):
    pass


class NotFoundSession(Exception):
    pass


class PayloadForJWTDoesNotContainedUserId(Exception):
    pass


class CreateSessionError(Exception):
    pass


class CreateUserError(Exception):
    pass


class UserNotExistError(Exception):
    pass


class PasswordInvalidError(Exception):
    pass


class NotEqualSignatureTokens(Exception):
    pass


class NotEqualUserIdTokenAndDb(Exception):
    pass
