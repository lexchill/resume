import jwt
from time import time

from uuid import uuid4

from ..schemas.jwt import AccessRefreshToken
from .exceptions import PayloadForJWTDoesNotContainedUserId


class TokenJWT:
    """
    This class is responsible for generating and decrypting tokens
    """

    def __init__(self, jwt_private: str, jwt_public: str, jwt_algorithm: str,
                 jwt_ttl: int = 3600, length_signature: int = 10):

        self.__jwt_private = jwt_private
        self.__jwt_ttl = jwt_ttl
        self.__jwt_public = jwt_public
        self.__jwt_algorithm = jwt_algorithm
        self.__length_signature = length_signature

    @property
    def jwt_public(self):
        return self.__jwt_public

    @property
    def jwt_algorithm(self):
        return self.__jwt_algorithm

    @property
    def jwt_ttl(self):
        return self.__jwt_ttl

    @property
    def length_signature(self):
        return self.__length_signature

    def __create_access_token(self, payload: dict) -> str:
        return jwt.encode(payload, self.__jwt_private, algorithm=self.jwt_algorithm)

    def __create_refresh_token(self, access_token: str) -> str:
        return uuid4().hex + access_token[-self.length_signature:]

    def check_signature_from_refresh_in_access(self, access_token: str, refresh_token: str) -> bool:
        if access_token[-self.length_signature:] != refresh_token[-self.length_signature:]:
            return False
        return True

    def decode_jwt(self, access_token: str) -> dict:
        return jwt.decode(access_token, self.jwt_public, algorithms=[self.jwt_algorithm],
                          options={'verify_signature': True})

    def access_token_is_alive(self, access_token) -> bool:
        return True if self.decode_jwt(access_token).get("expires", time()) > time() else False

    def create_new_jwt(self, **kwargs) -> AccessRefreshToken:
        if kwargs.get("expires", None) is None:
            payload = {
                "expires": time() + self.jwt_ttl
            }
        else:
            payload = {}

        if kwargs.get("user_id", None) is None:
            raise PayloadForJWTDoesNotContainedUserId("Payload for JWT does not have key: user_id")

        payload.update(kwargs)
        access_token = self.__create_access_token(payload=payload)
        return AccessRefreshToken(access_token=access_token,
                                  refresh_token=self.__create_refresh_token(access_token=access_token))
