from enum import Enum


class WebKeys(Enum):
    token_type = "Bearer"
    http_header_auth = "Authorization"
    access_token = "access_token"
    refresh_token = "refresh_token"
