from typing import Optional
from fastapi import HTTPException
from fastapi.requests import Request
from fastapi.security.base import SecurityBase
from fastapi.openapi.models import HTTPBase as HTTPBaseModel
from fastapi.security.utils import get_authorization_scheme_param

from .enums import WebKeys
from ..schemas.jwt import AccessRefreshToken


class JWTAuth(SecurityBase):
    def __init__(self, auto_error: bool = True):
        self.scheme_name = WebKeys.token_type.value
        self.model = HTTPBaseModel(scheme=self.scheme_name)
        self.auto_error = auto_error

    async def __call__(self, request: Request) -> Optional[AccessRefreshToken]:
        header_authorization: str = request.headers.get(WebKeys.http_header_auth.value, None)
        refresh_token: str = request.cookies.get(WebKeys.refresh_token.value, None)

        scheme_access, access_token = get_authorization_scheme_param(header_authorization)

        if not refresh_token or not scheme_access or scheme_access.lower() != self.scheme_name.lower():
            if self.auto_error:
                raise HTTPException(
                    status_code=401, detail="Not authenticated"
                )
            else:
                return None

        return AccessRefreshToken(access_token=access_token, refresh_token=refresh_token)


auth = JWTAuth(auto_error=True)
