import logging
from logging.handlers import TimedRotatingFileHandler

from datetime import datetime

from .config import app_config


def get_logger():
    log_file_path = f'{app_config.dir_log}/{datetime.now().strftime("%d_%m_%Y")}.log'
    log_file_handler = TimedRotatingFileHandler(log_file_path, when='D', encoding='utf-8')
    log_formatter = logging.Formatter(
        '%(asctime)s — %(name)s — %(levelname)s — %(message)s'
    )
    log_file_handler.setFormatter(log_formatter)
    log_file_handler.setLevel(logging.WARNING)

    my_logger = logging.getLogger()
    my_logger.addHandler(log_file_handler)
    return my_logger


logger = get_logger()
