import re

from typing import Union

from .exceptions import InvalidDataBaseUrlException
from ..tools.databases import DataBase
from ..tools.databases.redis import RedisWrapper
from ..tools.databases.mongo.mongo_db import MongoDb
from ..tools.databases.postgresql.postgresql_db import PostgresqlDb


class DataBaseFactory:
    regex_mongo_db_url = re.compile(
        r'^mongodb:\/\/\w{1,10}:\w{1,20}@(\d{1,3}\.){3}\d{1,3}:\d{4,5}\/\w{1,15}$', flags=re.IGNORECASE)

    regex_postgres_db_url = re.compile(
        r'^postgresql:\/\/\w{1,10}:\w{1,20}@(\d{1,3}\.){3}\d{1,3}:\d{4,5}\/\w{1,15}$', flags=re.IGNORECASE)

    regex_redis_db_url = re.compile(
        r'^redis:\/\/(\w{1,10}:\w{1,20}@)?(\d{1,3}\.){3}\d{1,3}:\d{4,5}\/\d{1,2}$', flags=re.IGNORECASE)

    @classmethod
    def get_database(cls, db_url: str, db_name: str = None) -> Union[DataBase, RedisWrapper]:

        if cls.regex_mongo_db_url.match(db_url) is not None:
            return MongoDb(db_url=db_url, db_name=db_name)

        if cls.regex_postgres_db_url.match(db_url) is not None:
            return PostgresqlDb(db_url=db_url, db_name=db_name)

        if cls.regex_redis_db_url.match(db_url) is not None:
            return RedisWrapper(db_url=db_url)

        raise InvalidDataBaseUrlException(f"For url = {db_url} not found DB")
