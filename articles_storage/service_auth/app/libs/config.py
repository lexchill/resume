import os
import sys

from configparser import ConfigParser

from .exceptions import NotFoundConfigDB


class Config:

    def __init__(self, path_config_file: str):
        config = ConfigParser()
        config.read(path_config_file)

        self.__jwt_private = open(config["JWT"]["path_private_key"]).read()
        self.__jwt_public = open(config["JWT"]["path_public_key"]).read()
        self.__jwt_algorithm = config["JWT"]["algorithm"]
        self.__jwt_ttl = int(config["JWT"]["ttl"])

        self.__redis = config["REDIS"]

        self.__log_dir = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), config["APP"]["log_dir_name"])
        if not os.path.exists(self.__log_dir):
            os.mkdir(self.__log_dir)

        if "mongo" in config["APP"]["db_type"].lower():
            self.__db = config["MONGODB"]
            self.__db_prefix = config["MONGODB"]["prefix_url"]
        elif "postgres" in config["APP"]["db_type"].lower():
            self.__db = config["POSTGRES"]
            self.__db_prefix = config["POSTGRES"]["prefix_url"]
        else:
            raise NotFoundConfigDB(f"Unknown type db {config['APP']['db_type']}")

    @property
    def dir_log(self):
        return self.__log_dir

    @property
    def jwt_private(self):
        return self.__jwt_private

    @property
    def jwt_public(self):
        return self.__jwt_public

    @property
    def jwt_algorithm(self):
        return self.__jwt_algorithm

    @property
    def jwt_ttl(self):
        return self.__jwt_ttl

    @property
    def db_url(self):
        return f"{self.__db_prefix}://{self.__db['login']}:{self.__db['password']}@{self.__db['ip']}:{self.__db['port']}/{self.__db['db_name']}"

    @property
    def redis_url(self):
        return f"{self.__redis['prefix_url']}://{self.__redis['login']}:{self.__redis['password']}@{self.__redis['ip']}:{self.__redis['port']}/{self.__redis['db_name']}"

    @property
    def db_name(self):
        return self.__db['db_name']


app_config = Config(path_config_file="settings.ini")
