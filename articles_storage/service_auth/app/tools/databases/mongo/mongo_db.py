import hashlib

import pymongo

from .. import DataBase


class MongoDb(DataBase):
    """
    Class for interaction with Mongo DB
    """

    def __init__(self, db_url: str, db_name: str):
        super().__init__(db_url, db_name)

        self._client = pymongo.MongoClient(self._db_url)
        self._db = self._client.get_database(db_name)

        self.users_collection = "users"

    def __new__(cls, db_url: str, db_name: str):
        # todo Желательно сделать чек коннект по-другому с помощью ConnectionFailure
        test_client = pymongo.MongoClient(db_url)
        test_client.server_info()

        assert test_client.get_database(db_name) is not None, f"Database with name = {db_name} is not exist"

        return super().__new__(cls)

    def insert_user(self, first_name: str, last_name: str, email: str, password: str):
        return str(
            self._db.get_collection(self.users_collection).insert_one(
                {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email": email,
                    "password": hashlib.md5(password.encode('utf-8')).hexdigest()
                }
            ).inserted_id
        )

    def check_exist_user_by_email(self, email: str) -> bool:
        return False if self._db.get_collection(
            self.users_collection).find_one({"email": email}) is None else True

    def get_user_by_email(self, email: str) -> dict:
        return self._db.get_collection(
            self.users_collection).find_one({"email": email})

