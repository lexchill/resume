class DataBase:

    def __init__(self, db_url: str, db_name: str):
        self._db_url: str = db_url
        self._db_name: str = db_name

    def insert_user(self, *args, **kwargs):
        pass

    def insert_session(self, *args, **kwargs):
        pass

    def update_session(self, *args, **kwargs):
        pass

    def delete_session(self, *args, **kwargs):
        pass

    def check_exist_user_by_email(self, email: str):
        pass

    def check_exists_session(self):
        pass

    def get_session(self, *args, **kwargs):
        pass

    def get_user_by_email(self, email: str) -> dict:
        pass
