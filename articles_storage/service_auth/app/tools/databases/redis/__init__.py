import redis


class RedisWrapper:

    def __init__(self, db_url: str):
        self.__conn_pool = redis.ConnectionPool.from_url(db_url, db=0, socket_connect_timeout=3,
                                                         health_check_interval=5)
        self._db = redis.Redis(connection_pool=self.__conn_pool)

    def __new__(cls, db_url: str):
        db = redis.from_url(db_url)
        db.ping()
        return super().__new__(cls)

    def __del__(self):
        self._db.flushdb()
        self._db.close()

    def __call__(self, *args, **kwargs):
        pass

    def set_key_value(self, key: str, value: str) -> bool:
        return self._db.set(name=key, value=value)

    def get_value_by_key(self, key: str):
        value = self._db.get(name=key)
        if isinstance(value, bytes):
            return value.decode("utf-8")
        else:
            return value

    def rename_key(self, key: str, new_key: str) -> bool:
        return self._db.rename(src=key, dst=new_key)

    def delete_key(self, key: str) -> bool:
        return True if self._db.delete(key) == 1 else False
