from pydantic import BaseModel, Field


class LoginSchema(BaseModel):
    email: str = Field(...,
                       regex=r"^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$",
                       description="User email",
                       max_length=100)
    password: str = Field(...,
                          description="User password",
                          min_length=6)

    class Config:
        schema_extra = {
            "example": {
                "email": "test@mail.com",
                "password": "test12"
            }
        }


class SignUpSchema(BaseModel):
    first_name: str = Field(..., description="User first name", min_length=3, max_length=20)
    last_name: str = Field(..., description="User last name", min_length=3, max_length=20)
    email: str = Field(...,
                       regex=r"^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$",
                       description="User email",
                       max_length=100)
    password: str = Field(...,
                          description="User password",
                          min_length=6)

    class Config:
        schema_extra = {
            "example": {
                "first_name": "test",
                "last_name": "test",
                "email": "test@mail.com",
                "password": "test12"
            }
        }
