from pydantic import BaseModel, Field


class RefreshToken(BaseModel):
    refresh_token: str = Field(..., description="Refresh token")


class AccessToken(BaseModel):
    access_token: str = Field(..., description="Access token")


class AccessRefreshToken(RefreshToken, AccessToken):
    pass
