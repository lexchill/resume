from fastapi import APIRouter, Body, Depends

from ..schemas.auth import LoginSchema, SignUpSchema
from ..controllers.auth_control import AuthControl
from ..schemas.jwt import AccessRefreshToken, AccessToken
from ..libs.depends import auth


router = APIRouter(
    prefix="/auth",
    tags=["auth"]
)


@router.post("/sign_up", response_model=AccessToken)
async def sign_up(user: SignUpSchema = Body(...)):
    return AuthControl.sign_up(**user.dict())


@router.post("/login", response_model=AccessToken)
async def login(user: LoginSchema = Body(...)):
    return AuthControl.login(email=user.email, password=user.password)


@router.post("/refresh", response_model=AccessToken)
async def refresh(current_jwt: AccessRefreshToken = Depends(auth)):
    return AuthControl.refresh_tokens(current_jwt)


@router.post("/logout")
async def logout(current_jwt: AccessRefreshToken = Depends(auth)):
    return AuthControl.logout(refresh_token=current_jwt.refresh_token)
