from fastapi import FastAPI

from .routers.authorization import router


app_auth = FastAPI()
app_auth.include_router(router)
