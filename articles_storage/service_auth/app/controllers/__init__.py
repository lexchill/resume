from functools import wraps
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from jwt.exceptions import DecodeError, InvalidSignatureError

from ..libs.exceptions import *
from ..libs.logging import logger
from ..libs.enums import WebKeys
from ..schemas.jwt import AccessRefreshToken


def logging(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (NotEqualSignatureTokens, PasswordInvalidError, NotEqualUserIdTokenAndDb,
                DecodeError, InvalidSignatureError) as err:
            logger.warning(repr(err))
            raise HTTPException(status_code=401, detail="Not authenticated")

        except (NotFoundSession, UserNotExistError) as err:
            logger.warning(repr(err))
            raise HTTPException(status_code=404, detail="Not found")

        except (PayloadForJWTDoesNotContainedUserId, CreateSessionError, CreateUserError, Exception) as err:
            logger.error(repr(err), exc_info=True)
            raise HTTPException(status_code=500, detail="Error")

    return wrapper


def http_json(auth_control_func):
    """
    It is decorator for setting http response with need cookies and json data
    """

    def wrapper(*args, **kwargs) -> JSONResponse:
        jwt: AccessRefreshToken = auth_control_func(*args, **kwargs)
        response = JSONResponse(status_code=200, content={WebKeys.access_token.value: jwt.access_token})
        response.set_cookie(
            key=WebKeys.refresh_token.value,
            value=jwt.refresh_token,
            httponly=True,
            max_age=900
        )
        return response

    return wrapper
