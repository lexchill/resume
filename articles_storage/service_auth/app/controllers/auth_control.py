import hashlib

from time import time
from fastapi.responses import JSONResponse

from ..libs.exceptions import *
from ..libs.enums import WebKeys
from ..libs.db import DataBaseFactory
from ..libs.token import TokenJWT
from ..libs.config import app_config
from ..schemas.jwt import AccessRefreshToken
from ..controllers import logging, http_json


class AuthControl:
    db = DataBaseFactory.get_database(db_url=app_config.db_url, db_name=app_config.db_name)
    redis = DataBaseFactory.get_database(db_url=app_config.redis_url)
    token_jwt = TokenJWT(jwt_private=app_config.jwt_private, jwt_public=app_config.jwt_public,
                         jwt_algorithm=app_config.jwt_algorithm, jwt_ttl=app_config.jwt_ttl)

    @classmethod
    @logging
    @http_json
    def sign_up(cls, **kwargs) -> AccessRefreshToken:
        assert kwargs.get("email", None) is not None, "User info not contained Email!"

        if cls.db.check_exist_user_by_email(email=kwargs["email"]) is True:
            raise CreateUserError(f"User with email = {kwargs['email']} is exist")

        user_id = cls.db.insert_user(**kwargs)
        jwt = cls.token_jwt.create_new_jwt(user_id=user_id)

        if cls.redis.set_key_value(jwt.refresh_token, user_id) is False:
            raise CreateSessionError("Refresh token was not insert in Redis")

        return jwt

    @classmethod
    @logging
    @http_json
    def login(cls, email: str, password: str) -> AccessRefreshToken:
        exist_user = cls.db.get_user_by_email(email=email)
        if exist_user is None:
            raise UserNotExistError("User is not exist. Invalid login.")

        if hashlib.md5(password.encode('utf-8')).hexdigest() != exist_user.get("password", None):
            raise PasswordInvalidError("Invalid password")

        jwt = cls.token_jwt.create_new_jwt(user_id=str(exist_user["_id"]))

        if cls.redis.set_key_value(jwt.refresh_token, str(exist_user["_id"])) is False:
            raise CreateSessionError("Refresh token was not insert in Redis")

        return jwt

    @classmethod
    @logging
    @http_json
    def refresh_tokens(cls, current_jwt: AccessRefreshToken) -> AccessRefreshToken:
        if not cls.token_jwt.check_signature_from_refresh_in_access(current_jwt.access_token,
                                                                    current_jwt.refresh_token):
            raise NotEqualSignatureTokens("Tokens signature not equal")

        jwt_info = cls.token_jwt.decode_jwt(access_token=current_jwt.access_token)

        if jwt_info.get("expires", time()) > time():
            return AccessRefreshToken(access_token=current_jwt.access_token, refresh_token=current_jwt.refresh_token)

        user_id = cls.redis.get_value_by_key(key=current_jwt.refresh_token)

        if user_id is None:
            raise NotFoundSession(f"Session for refresh_token = {current_jwt.refresh_token} is not found")

        if jwt_info.get("user_id", 0) != user_id:
            raise NotEqualUserIdTokenAndDb("User_id in access token not equal user_id in session on redis")

        new_jwt = cls.token_jwt.create_new_jwt(user_id=user_id)

        if cls.redis.rename_key(key=current_jwt.refresh_token, new_key=new_jwt.refresh_token) is False:
            raise CreateSessionError("Refresh token was not update in Redis")

        return new_jwt

    @classmethod
    @logging
    def logout(cls, refresh_token: str) -> JSONResponse:
        if cls.redis.delete_key(key=refresh_token):
            response = JSONResponse(status_code=200)
        else:
            response = JSONResponse(status_code=501)

        response.delete_cookie(key=WebKeys.refresh_token.value)
        return response
