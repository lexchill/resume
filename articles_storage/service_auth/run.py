import uvicorn

if __name__ == "__main__":
    uvicorn.run("app.app:app_auth", host="127.0.0.1", port=8081, reload=True)
