import pytest
import hashlib

from typing import Dict
from collections import defaultdict
from uuid import uuid4

from service_auth.app import controllers
from service_auth.app.libs import db


class MockMongo:
    def __init__(self, *args, **kwargs):
        self.db: dict = defaultdict(int)

    def check_exist_user_by_email(self, email: str) -> bool:
        return any(map(lambda item: True if item.get('email', '') == email else False, self.db.values()))

    def insert_user(self, **kwargs) -> str:
        kwargs['_id'] = uuid4().hex
        kwargs['password'] = hashlib.md5(kwargs.get('password', '').encode('utf-8')).hexdigest()
        self.db[len(self.db.keys())] = kwargs
        return kwargs['_id']

    def get_user_by_email(self, email: str) -> dict:
        for user in self.db.values():
            if user.get('email', '') == email:
                return user


class MockRedis:
    def __init__(self, *args, **kwargs):
        self.db: Dict[str, str] = defaultdict(str)

    def set_key_value(self, key: str, value: str) -> bool:
        if self.db.get(key, None) is None:
            self.db[key] = value
            return True
        else:
            return False

    def get_value_by_key(self, key: str) -> str:
        return self.db.get(key, '')

    def rename_key(self, key: str, new_key: str) -> bool:
        value = self.db.get(key, '')
        if self.db.pop(key, None) is None:
            return False
        self.db[new_key] = value
        return True

    def delete_key(self, key: str) -> bool:
        return False if self.db.pop(key, None) is None else True


class MockDatabaseFactory:
    @classmethod
    def get_database(cls, *args, **kwargs):
        if any(map(lambda arg: True if "mongo" in arg else False, list(args) + list(kwargs.values()))):
            return MockMongo()

        if any(map(lambda arg: True if "redis" in arg else False, list(args) + list(kwargs.values()))):
            return MockRedis()


def mock_decorator(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


@pytest.fixture(scope="module")
def get_users():
    return [
        {
            "first_name": "test",
            "last_name": "test",
            "email": "test123@mail.com",
            "password": "test"
        },
        {
            "first_name": "test",
            "last_name": "test",
            "email": "test1234@mail.com",
            "password": "test"
        },
        {
            "first_name": "test",
            "last_name": "test",
            "email": "test1235@mail.com",
            "password": "test"
        }
    ]


@pytest.fixture(scope='module')
def monkeypatch_session():
    from _pytest.monkeypatch import MonkeyPatch
    monkey_session = MonkeyPatch()
    yield monkey_session
    monkey_session.undo()


@pytest.fixture(scope="module", autouse=True)
def mock_all_db_for_test_controller(monkeypatch_session):
    monkeypatch_session.setattr(db, "DataBaseFactory", MockDatabaseFactory)
    monkeypatch_session.setattr(controllers, "http_json", mock_decorator)
    monkeypatch_session.setattr(controllers, "logging", mock_decorator)
