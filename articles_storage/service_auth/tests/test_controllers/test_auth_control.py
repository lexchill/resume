from service_auth.app.schemas.jwt import AccessRefreshToken


def test_sign_up(get_users):
    from service_auth.app.controllers.auth_control import AuthControl
    for user in get_users:
        jwt = AuthControl.sign_up(**user)
        assert isinstance(jwt, AccessRefreshToken)


def test_login(get_users):
    from service_auth.app.controllers.auth_control import AuthControl
    for user in get_users:
        jwt = AuthControl.login(user["email"], user["password"])
        assert isinstance(jwt, AccessRefreshToken)
