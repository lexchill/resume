import pytest
import hashlib
import docker
import socket

from uuid import uuid4


@pytest.fixture(scope='session')
def session_id():
    return uuid4().hex


@pytest.fixture(scope='module')
def unused_port():
    def search_unused_port():
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind(("127.0.0.1", 0))
            return sock.getsockname()[1]

    return search_unused_port


@pytest.fixture(scope='module')
def my_docker():
    return docker.Client(version='auto')


@pytest.fixture(scope="module")
def get_user():
    return {
        "first_name": "test",
        "last_name": "test",
        "email": "test@mail.com",
        "password": hashlib.md5("test".encode('utf-8')).hexdigest()
    }


pytest_plugins = ["redis_fixtures", "mongo_fixtures"]
