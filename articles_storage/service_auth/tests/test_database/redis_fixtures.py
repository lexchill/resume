import pytest
import redis
import time

from service_auth.app.tools.databases.redis import RedisWrapper


def redis_container_is_start(port):
    timeout = 0.001
    for i in range(100):
        try:
            client = redis.Redis(host="127.0.0.1", port=port, db=0)
        except redis.ConnectionError:
            time.sleep(timeout)
        else:
            client.close()
            return
    raise RuntimeError("Cannot connect to redis")


@pytest.fixture(scope="module")
def redis_server(unused_port, session_id, my_docker):
    # mydocker.pull('redis')
    port = unused_port()
    container = my_docker.create_container(
        image='redis:3-32bit',
        name='test-redis-{}'.format(session_id),
        ports=[6379],
        detach=True,
        host_config=my_docker.create_host_config(
            port_bindings={6379: port}))
    my_docker.start(container=container['Id'])
    redis_container_is_start(port)
    container['redis_port'] = port
    yield container
    my_docker.kill(container=container['Id'])
    my_docker.remove_container(container['Id'])


@pytest.fixture(scope='module')
def redis_client(redis_server):
    client = RedisWrapper(db_url=f"redis://127.0.0.1:{redis_server['redis_port']}/0")
    # client = redis.Redis(host="127.0.0.1", port=redis_server["redis_port"], db=0)
    return client
