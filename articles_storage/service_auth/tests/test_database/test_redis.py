from service_auth.app.tools.databases.redis import RedisWrapper
import pytest


@pytest.mark.parametrize("key, value", [
    ('key', 'value'),
    ('key1', 'value1'),
    ('key2', 'value2')
])
def test_set_key_value_func(redis_client: RedisWrapper, key: str, value: str):
    assert redis_client.set_key_value(key=key, value=value) is True


@pytest.mark.parametrize("key, value", [
    ('key', 'value'),
    ('key1', 'value1'),
    ('key2', 'value2')
])
def test_get_value_by_key_func(redis_client: RedisWrapper, key: str, value: str):
    assert redis_client.get_value_by_key(key=key) == value


@pytest.mark.parametrize("key, value", [
    ('key', 'value'),
    ('key1', 'value1'),
    ('key2', 'value2')
])
def test_rename_key_func(redis_client: RedisWrapper, key: str, value: str):
    assert redis_client.rename_key(key=key, new_key=f"new_{key}") is True
    assert redis_client.get_value_by_key(key=f"new_{key}") == value


@pytest.mark.parametrize("key", [
    'key',
    'key1',
    'key2'
])
def test_delete_not_existkey_func(redis_client: RedisWrapper, key: str):
    assert redis_client.delete_key(key=key) is False
    assert redis_client.get_value_by_key(key=key) is None


@pytest.mark.parametrize("key", [
    'new_key',
    'new_key1',
    'new_key2'
])
def test_delete_exist_key_func(redis_client: RedisWrapper, key: str):
    assert redis_client.delete_key(key=key) is True
    assert redis_client.get_value_by_key(key=key) is None
