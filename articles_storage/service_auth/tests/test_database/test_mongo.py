import pytest

from service_auth.app.tools.databases.mongo.mongo_db import MongoDb


def test_insert_user_func(mongo_client: MongoDb, get_user: dict):
    assert isinstance(mongo_client.insert_user(**get_user), str)


def test_insert_user_func2(mongo_client: MongoDb, get_user: dict):
    assert mongo_client.check_exist_user_by_email(get_user.get("email", "")) is True


@pytest.mark.parametrize("email, result", [
    ('test@mail.com', True)
])
def test_search_exist_email_func(mongo_client: MongoDb, email: str, result: bool):
    assert mongo_client.check_exist_user_by_email(email) is result
    assert mongo_client.get_user_by_email(email).get("email", None) == email


@pytest.mark.parametrize("email, result", [
    ('tthest@mail.com', False),
    ('tthemail.com', False)
])
def test_search_not_exist_email_func(mongo_client: MongoDb, email: str, result: bool):
    assert mongo_client.check_exist_user_by_email(email) is result
