import pytest
import pymongo
import time
from pymongo.errors import ConnectionFailure

from service_auth.app.tools.databases.mongo.mongo_db import MongoDb


def mongo_container_is_start(port):
    timeout = 0.001
    for i in range(100):
        try:
            client = pymongo.MongoClient(host="127.0.0.1", port=port)
            client.server_info()
        except ConnectionFailure:
            timeout *= 2
            time.sleep(timeout)
        else:
            client.close()
            return
    raise RuntimeError("Cannot connect to redis")


@pytest.fixture(scope='module')
def mongo_server(unused_port, session_id, my_docker):
    # my_docker.pull('mongo')
    port = unused_port()
    container = my_docker.create_container(
        image='mongo:latest',
        name='test-mongo-{}'.format(session_id),
        ports=[27017],
        detach=True,
        host_config=my_docker.create_host_config(
            port_bindings={27017: port}))
    my_docker.start(container=container['Id'])
    mongo_container_is_start(port)
    container['mongo_port'] = port
    yield container
    my_docker.kill(container=container['Id'])
    my_docker.remove_container(container['Id'])


@pytest.fixture(scope='module')
def mongo_client(mongo_server):
    client = MongoDb(db_url=f"mongodb://127.0.0.1:{mongo_server['mongo_port']}", db_name="test")
    return client
