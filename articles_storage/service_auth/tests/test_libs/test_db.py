import pytest

from service_auth.app.libs.db import DataBaseFactory, MongoDb, RedisWrapper, PostgresqlDb
from service_auth.app.libs.exceptions import InvalidDataBaseUrlException


@pytest.mark.parametrize("expected_exception, db_url", [
    (InvalidDataBaseUrlException, 'postgresql://test:test@1234.124.52.235:25/wg'),
    (InvalidDataBaseUrlException, 'redis://test:test@134.124.52.235:2523/wg'),
    (InvalidDataBaseUrlException, 'mongodb://test@124.124.52.235:2353/wg'),
    (InvalidDataBaseUrlException, 'postgresql://test:test@123.124.52.235:252353/wg')
])
def test_get_database_with_error(expected_exception, db_url):
    with pytest.raises(expected_exception):
        DataBaseFactory.get_database(db_url=db_url)


@pytest.mark.parametrize("db_url, type_db", [
    ('postgresql://test:test@123.124.52.235:2546/wg', PostgresqlDb),
    ('redis://test:test@134.124.52.235:2523/0', RedisWrapper),
    ('mongodb://test:test@124.124.52.235:2353/wg', MongoDb),
    ('postgresql://test:test@123.124.52.235:2353/wg', PostgresqlDb)
])
def test_get_database(db_url, type_db):
    assert isinstance(DataBaseFactory.get_database(db_url=db_url), type_db)
