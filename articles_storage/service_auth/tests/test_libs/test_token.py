import pytest
import jwt as lib_jwt

from time import time

from service_auth.app.libs.token import TokenJWT
from service_auth.app.libs.config import Config
from service_auth.app.libs.exceptions import PayloadForJWTDoesNotContainedUserId
from service_auth.app.schemas.jwt import AccessRefreshToken


@pytest.fixture(scope="function")
def payload():
    return {"expires": time(), "user_id": 123456}


@pytest.fixture(scope="session")
def example_config() -> Config:
    return Config(path_config_file="settings.ini")


@pytest.fixture(scope="session")
def example_jwt(example_config) -> TokenJWT:
    return TokenJWT(jwt_private=example_config.jwt_private, jwt_public=example_config.jwt_public,
                    jwt_algorithm=example_config.jwt_algorithm, jwt_ttl=example_config.jwt_ttl)


def test_create_jwt_with_error(example_jwt: TokenJWT, payload: dict):
    with pytest.raises(PayloadForJWTDoesNotContainedUserId):
        del payload["user_id"]
        example_jwt.create_new_jwt(**payload)


def test_init_example_jwt(example_jwt: TokenJWT, example_config: Config):
    assert example_config.jwt_ttl == example_jwt.jwt_ttl
    assert example_config.jwt_algorithm == example_jwt.jwt_algorithm
    assert example_config.jwt_public == example_jwt.jwt_public


def test_type_create_jwt(example_jwt: TokenJWT, payload: dict):
    jwt = example_jwt.create_new_jwt(**payload)
    assert isinstance(jwt, AccessRefreshToken)


def test_access_token_jwt(example_jwt: TokenJWT, example_config: Config, payload: dict):
    jwt = example_jwt.create_new_jwt(**payload)
    assert jwt.access_token == lib_jwt.encode(payload, example_config.jwt_private,
                                              algorithm=example_config.jwt_algorithm)


def test_relations_between_tokens_in_jwt(example_jwt: TokenJWT, payload: dict):
    jwt = example_jwt.create_new_jwt(**payload)
    tokens_is_binding = example_jwt.check_signature_from_refresh_in_access(jwt.access_token, jwt.refresh_token)
    if jwt.access_token[-example_jwt.length_signature:] != jwt.refresh_token[-example_jwt.length_signature:]:
        assert tokens_is_binding is False
    else:
        assert tokens_is_binding is True


def test_access_token_not_is_alive(example_jwt: TokenJWT, payload: dict):
    jwt = example_jwt.create_new_jwt(**payload)
    assert example_jwt.access_token_is_alive(jwt.access_token) is False


def test_access_token_is_alive(example_jwt: TokenJWT, payload: dict):
    payload["expires"] = time() + 1000
    jwt = example_jwt.create_new_jwt(**payload)
    assert example_jwt.access_token_is_alive(jwt.access_token) is True
