import pytest
import pymongo
import redis


class MockPostgresDb:

    def __init__(self, *args, **kwargs):
        pass


class MockMongoDb:

    def __init__(self, *args, **kwargs):
        pass

    @classmethod
    def get_database(cls, *args, **kwargs):
        return True

    @classmethod
    def server_info(cls, *args, **kwargs):
        pass


class MockRedisWrapper:

    def __init__(self, *args, **kwargs):
        pass

    @classmethod
    def ping(cls, *args, **kwargs):
        pass

    @classmethod
    def flushdb(cls, *args, **kwargs):
        pass

    @classmethod
    def close(cls, *args, **kwargs):
        pass


@pytest.fixture(scope='module')
def monkeypatch_session():
    from _pytest.monkeypatch import MonkeyPatch
    monkey_session = MonkeyPatch()
    yield monkey_session
    monkey_session.undo()


@pytest.fixture(scope="module", autouse=True)
def mock_all_db_for_test_libs(monkeypatch_session):
    monkeypatch_session.setattr(pymongo, "MongoClient", MockMongoDb)
    monkeypatch_session.setattr(redis, "from_url", MockRedisWrapper)
