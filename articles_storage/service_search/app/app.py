from fastapi import FastAPI

from .routes.searcher import search_router

app_search = FastAPI()
app_search.include_router(search_router)
