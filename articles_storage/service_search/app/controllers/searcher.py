from typing import List

from ..libs.db import DataBase


class Searcher:
    @staticmethod
    def search_article_by_subject(list_word: List[str]):
        return DataBase.search_article_by_subject(list_word)

    @staticmethod
    def search_article_by_content(list_word: List[str]):
        return DataBase.search_article_by_content(list_word)

    @staticmethod
    def search_everywhere(sub_list_word: List[str], con_list_word: List[str]):
        return DataBase.search_everywhere(sub=sub_list_word, con=con_list_word)
