from typing import List
from fastapi import APIRouter, Body, Depends

from ..libs.depends import auth
from ..controllers.searcher import Searcher

search_router = APIRouter(
    prefix="/articles",
    tags=["searchers"]
)


@search_router.post("/search/")
async def search_articles(sub: List[str] = Body(None), con: List[str] = Body(None),
                          user_is_auth: bool = Depends(auth)):
    if sub and con:
        a = Searcher.search_everywhere(sub_list_word=sub, con_list_word=con)
    elif sub:
        res = Searcher.search_article_by_subject(list_word=sub)
        for i in res:
            del i['_id']
        return res

    elif con:
        a = Searcher.search_article_by_content(list_word=sub)
    else:
        return '1111'
