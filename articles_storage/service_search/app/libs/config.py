from configparser import ConfigParser


class Config:

    def __init__(self, path_config_file: str):
        config = ConfigParser()
        config.read(path_config_file)

        self.__db = config["MONGODB"]
        self.__db_prefix = config["MONGODB"]["prefix_url"]

        self.__jwt_public = open(config["JWT"]["path_public_key"]).read()
        self.__jwt_algorithm = config["JWT"]["algorithm"]

    @property
    def db_url(self):
        return f"{self.__db_prefix}://{self.__db['login']}:{self.__db['password']}@{self.__db['ip']}:{self.__db['port']}/{self.__db['db_name']}"

    @property
    def db_name(self):
        return self.__db['db_name']

    @property
    def jwt_public(self):
        return self.__jwt_public

    @property
    def jwt_algorithm(self):
        return self.__jwt_algorithm


app_config = Config(path_config_file="settings.ini")
