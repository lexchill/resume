from typing import List

from ..models.articles import CollectionArticles


class DataBase:
    @staticmethod
    def search_article_by_subject(list_word: List[str]):
        return CollectionArticles.search_by_subject(" ".join(list_word))

    @staticmethod
    def search_article_by_content(list_word: List[str]):
        return CollectionArticles.search_by_content(" ".join(list_word))

    @staticmethod
    def search_everywhere(sub: List[str], con: List[str]):
        return CollectionArticles.search_everywhere(text_sub=" ".join(sub), text_con=" ".join(con))