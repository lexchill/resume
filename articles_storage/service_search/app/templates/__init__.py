from bs4 import BeautifulSoup
from bs4 import Tag

from ..libs.exceptions import NotFoundHtmlArticle


class WebPageTemplate:
    def __init__(self, url: str, html5_text: str):
        assert html5_text is not None and html5_text != '', "Text page is empty"
        html5 = BeautifulSoup(html5_text, 'html.parser')

        self._head: Tag = html5.find("head")
        self._title: Tag = self._head.find("title")
        self._body: Tag = html5.find("body")

        self.url: str = url
        self.text_article: str = ''

    def __iter__(self):
        yield from {
            "url": self.url,
            "subject": self.get_text_title_page,
            "content": self.get_text_article,
        }.items()

    @property
    def get_text_title_page(self) -> str:
        return self.convert_html5_to_text(self._title)

    @property
    def get_text_body_page(self) -> str:
        return self.convert_html5_to_text(self._body)

    @property
    def get_text_article(self):
        if self.text_article == '':
            html_article = self.get_html_article()
            if html_article:
                return self.convert_html5_to_text(html5_tag=html_article)
            else:
                raise NotFoundHtmlArticle(f'URL = {self.url}, text article was not found!')
        else:
            return self.text_article

    @staticmethod
    def get_names_all_tags(html5_tag: Tag) -> set:
        return set(
            map(lambda tag: tag.name, html5_tag.find_all(lambda item: True if isinstance(item, Tag) else False))
        )

    @staticmethod
    def convert_html5_to_text(html5_tag: Tag) -> str:
        return html5_tag.get_text().replace("\n", "").replace("\r", "")

    def get_html_article(self) -> Tag:
        pass

