from pymongo import MongoClient

from ..libs.config import app_config


class CollectionArticles:
    """
    Class for interaction with Mongo DB
    """
    _client = MongoClient(app_config.db_url)
    _client.server_info()

    assert _client.get_database(
        app_config.db_name) is not None, f"Database with name = {app_config.db_name} is not exist"

    _db = _client.get_database(app_config.db_name)
    _collection_name = "articles"

    @classmethod
    def search_by_subject(cls, text: str):
        return [article for article in cls._db.get_collection(cls._collection_name).find(
            {"$text": {"$search": text}}, {"score": {"$meta": "textScore"}}).sort(
            [('score', {'$meta': 'textScore'})])]

    @classmethod
    def search_by_content(cls, text: str):
        pass

    @classmethod
    def search_everywhere(cls, text_sub: str, text_con: str):
        pass