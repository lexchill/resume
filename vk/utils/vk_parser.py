from user_agent import generate_user_agent
from bs4 import BeautifulSoup
import json

from vk.libs.requests import Request
from vk.libs.logger import logger

request_params_for_auth_vk = {
    'act': 'login',
    'role': 'al_frame',
    'expire': '',
    'recaptcha': '',
    'captcha_sid': '',
    'captcha_key': '',
    '_origin': 'https://vk.com',
    'ip_h': None,
    'lg_h': None,
    'ul': '',
    'email': None,
    'pass': None,
}

request_params_for_get_friends = {
    'act': 'load_friends_silent',
    'al': 1,
    'gid': 0,
    'id': None
}

class VkParser:
    """
    Класс получения данных из ВКонтакте с использованием прямого доступа
    """

    url_vk = 'https://vk.com/'
    url_vk_login = 'https://login.vk.com/?act=login'
    url_vk_get_friends = 'https://vk.com/al_friends.php'
    url_vk_get_mentions = 'https://vk.com/al_feed.php'

    def __init__(self, login: str, passwd: str):
        self.login = login
        self.passwd = passwd

        self.session = Request.get_new_session()
        self.session.headers = {'user-agent': generate_user_agent(device_type="all", os=('mac', 'linux'))}
        self.__auth_in_vk()

    def __auth_in_vk(self) -> bool:
        """
        Функция авторизации ВКонтакте
        :param 
        :return:
        """
        response = Request.request(method='GET', session=self.session, url=self.url_vk)
        if response:
            html = BeautifulSoup(response, 'html.parser')
            ip_h = html.find('input', attrs={'name': 'ip_h'})['value']
            lg_h = html.find('input', attrs={'name': 'lg_h'})['value']
            request_params_for_auth_vk['ip_h'] = ip_h
            request_params_for_auth_vk['lg_h'] = lg_h
            request_params_for_auth_vk['email'] = self.login
            request_params_for_auth_vk['pass'] = self.passwd
            response = Request.request(method='POST', session=self.session, url=self.url_vk_login, data=request_params_for_auth_vk)
            
            if response:
                text_to_look_for = 'parent.onLogin'
                text_offset = len(text_to_look_for)
                
                response_info_start_ind = response.find(text_to_look_for) + text_offset
                response_info_end_ind = response.find('(', response_info_start_ind)

                response_info = response[response_info_start_ind : response_info_end_ind]

                if response_info == 'Done':
                    return True
        
        logger.warning('Ошибка авторизации ВКонтакте, логин: %s, пароль: %s', self.login, self.passwd)
        return False

    def get_friends(self, user_id: int):
        """
        Функция получения списка друзей пользователя ВКонтакте
        :param user_id: идентификатор пользователя ВК
        :return:
        """
        request_params_for_get_friends['id'] = user_id
        response = Request.request(method='GET', session=self.session, url=self.url_vk_get_friends, params=request_params_for_get_friends)
        response_json = json.loads(response[4:])
        
        if response_json:
            if response_json.get('payload', None):
                if type(response_json['payload']) is list:
                    if type(response_json['payload'][1]) is list:
                        if response_json['payload'][0] == 0:

                            if response_json['payload'][1][0].get('all', None):
                                friends_info_list = response_json['payload'][1][0]['all']
                                if type(friends_info_list) is list:
                                    if type(friends_info_list[0]) is list:
                                        if str(friends_info_list[0][0]).isdigit():
                                            return [int(friend_info[0]) for friend_info in friends_info_list]
                                            
                        elif response_json['payload'][0] == '8':
                            if response_json['payload'][1][0] == '"Access denied"':
                                logger.warning('Закрытый профиль, id: %d', user_id)
                                return None

        logger.warning('Ответ от ВКонтакте не соответствует формату, id: %d, ответ: %s', user_id, str(response_json))
        return None

    def get_user_mentions(self, user_id: int):
        """
        Функция получения упоминаний пользователя ВКонтакте
        :param user_id: идентификатор пользователя ВК
        :return:
        """
        request_params_for_get_mentions = {}
        request_params_for_get_mentions['obj'] = str(user_id)
        request_params_for_get_mentions['q'] = ''
        request_params_for_get_mentions['section'] = 'mentions'
        response = Request.request(method='GET', session=self.session, url=self.url_vk_get_mentions, params=request_params_for_get_mentions)
        
        html = BeautifulSoup(response, 'html.parser')
        mentions = []

        for feed_row in html.find_all('div', attrs={'class': 'feed_row'}):
            mention_info = dict.fromkeys([
                'mention_author_link', 'mention_author_id',
                'mention_author_name', 'mention_author_type',
                'mention_link', 'post_link', 'mention_type',
                'mention_text', 'post_owner_name', 'post_date'
                ], None)

            for post_author_info in feed_row.find_all('a', attrs={'class', 'author'}):
                mention_info['mention_author_link'] = post_author_info.get('href')

                data_from_id = post_author_info.get('data-from-id')
                if data_from_id.isdigit():
                    mention_info['mention_author_id'] = 'id' + data_from_id
                    mention_info['mention_author_type'] = 'user'
                elif data_from_id[0] == '-':
                    mention_info['mention_author_id'] = 'public' + data_from_id[1:]
                    mention_info['mention_author_type'] = 'public'
                else:
                    mention_info['mention_author_id'] = data_from_id
                    mention_info['mention_author_type'] = 'unknown'

                mention_info['mention_author_name'] = post_author_info.text
            
            post_date_info = feed_row.find('div', attrs={'class', 'post_date'}).text
            mention_info['post_date'] = feed_row.find('span', attrs={'class', 'rel_date'}).text
            if post_date_info != mention_info['post_date']:
                mention_info['post_owner_name'] = post_date_info[post_date_info.find('on') + 3 : post_date_info.rfind('post')]
            
            post_link_infos = feed_row.find_all('a', attrs={'class', 'post_link'})

            if len(post_link_infos) == 2:
                mention_info['mention_type'] = 'comment'
                mention_info['post_link'] = post_link_infos[0].get('href')
                mention_info['mention_link'] = post_link_infos[1].get('href')

            elif len(post_link_infos) == 1:
                mention_info['mention_type'] = 'post'
                mention_info['mention_link'] = post_link_infos[0].get('href')

            wall_text = feed_row.find('div', attrs={'class': 'wall_text'})
            mention_info['mention_text'] = wall_text.text

            mentions.append(mention_info)

        return mentions
