from bs4 import BeautifulSoup

from vk.libs.requests import Request
from vk.libs.enums import RequestType


class VkAssist:

    php_url = "https://vk.com/foaf.php"
    regvk_url = "https://regvk.com/id/"
    regvk_bpc = None

    @classmethod
    def get_user_info_by_nickname_regvk(cls, nickname: str):
        """
        Функция для получения id пользователя ВК от его никнейма через сайт regvk.com
        :param nickname: никнейм пользователя ВК
        :return:
        """
        if cls.regvk_bpc is None:
            bpc_response = Request.request(method='GET', url=cls.regvk_url, request_type=RequestType.regvk_request)
            html = BeautifulSoup(bpc_response, 'html.parser')
            bpc_data = html.find('script')
            if bpc_data:
                for part in bpc_data.string.split(';'):
                    if 'bpc=' in part:
                        cls.regvk_bpc = part[str.find(part, 'bpc=') + 4:]
                        break

        response = Request.send_regvk_request(url=cls.regvk_url, cookies={'bpc': cls.regvk_bpc},
                                              data={'link': nickname, 'button': 'Определить ID'})

        html = BeautifulSoup(response, 'html.parser')
        table_data = html.find('table')
        if table_data:
            list_row_table = table_data.find_all('td')
            return {
                'id': 'id' + list_row_table[1].text.split(' ')[-1],
                'fio': list_row_table[0].text if len(list_row_table) == 4 else None,
                'nickname': list_row_table[3].text.split(' ')[-1].split('/')[-1] if list_row_table[2].text.split(' ')[-1]
                                                                                    != list_row_table[3].text.split(' ')[-1] else None
            }

    @classmethod
    def get_info_from_php_script_by_id(cls, user_id: int):
        """
        Функция для получения минимального количества данных от php скрипта
        :param user_id: id пользователя в ВК
        :return php_request_result: словарь с информацией о пользователе
        """
        response = Request.send_php_request(url=cls.php_url, data={'id': user_id})
        response_tags = BeautifulSoup(response, 'lxml')
        php_request_result = dict.fromkeys(
            [
                'firstname', 'secondname', 'uri_primary', 'uri',
                'created', 'last_logged_in', 'image'
            ]
        )
        for tag in response_tags.find_all():
            if tag.name == 'ya:uri':
                if tag.get('ya:primary') == 'yes':
                    php_request_result['uri_primary'] = tag.get('rdf:resource')
                else:
                    php_request_result['uri'] = tag.get('rdf:resource')
            if tag.name == 'ya:firstname':
                php_request_result['firstname'] = tag.text
            if tag.name == 'ya:secondname':
                php_request_result['secondname'] = tag.text
            if tag.name == 'ya:created':
                php_request_result['created'] = tag.get('dc:date')
            if tag.name == 'ya:lastloggedin':
                php_request_result['last_logged_in'] = tag.get('dc:date')
            if tag.name == 'foaf:image' and tag.get('ya:primary') == 'yes':
                php_request_result['image'] = tag.get('rdf:about')

        return php_request_result
