import logging
import os
import sys


class Logger:

    logger = logging.getLogger('VK parser logger')
    log_dir = os.path.join(
        os.path.dirname(os.path.abspath(sys.argv[0])), 'logs')
    log_file = os.path.join(log_dir, 'log.txt')
    logger.setLevel(logging.INFO)
    log_handler = logging.FileHandler(log_file)
    log_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_handler.setFormatter(log_format)
    logger.addHandler(log_handler)

    @classmethod
    def get_logger(cls):
        return cls.logger

logger = Logger.get_logger()