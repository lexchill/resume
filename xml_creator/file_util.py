from os import path, mkdir, walk
import sys

from xml.etree import ElementTree
from random import choice, randint
from string import ascii_letters

DIR_XML_FILES = path.join(path.dirname(path.abspath(sys.argv[0])), 'xml_files')
if not path.exists(DIR_XML_FILES):
    mkdir(DIR_XML_FILES)

DIR_ARCH_FILES = path.join(path.dirname(path.abspath(sys.argv[0])), 'arch_files')
if not path.exists(DIR_ARCH_FILES):
    mkdir(DIR_ARCH_FILES)

DIR_CSV_FILES = path.join(path.dirname(path.abspath(sys.argv[0])), 'csv_files')
if not path.exists(DIR_CSV_FILES):
    mkdir(DIR_CSV_FILES)


class GenerateFile:

    @classmethod
    def generate_random_str(cls, reservation_id: str = None) -> str:
        """
        Функция для генерации рандомной строки
        :param reservation_id: значение тэга id для соблюдения условия уникальности
        """

        if reservation_id is None:
            return ''.join(choice(ascii_letters) for _ in range(5))
        else:
            new_str_value = ''.join(choice(ascii_letters) for _ in range(5))
            if new_str_value == reservation_id:
                return cls.generate_random_str(reservation_id=reservation_id)
            else:
                return new_str_value

    @classmethod
    def generate_tag(cls, tag_name: str, **attrs) -> ElementTree.Element:
        """
        Функция для генерации тэга
        :param tag_name: имя тэга
        :param attrs: словарь с атрибутами тэга и их значениями
        """
        return ElementTree.Element(tag_name, **attrs)

    @classmethod
    def generate_one_xml(cls) -> ElementTree.Element:
        """Функция для генерации одного xml документа с заданной структурой"""

        root = cls.generate_tag(tag_name='root')
        root.append(cls.generate_tag(tag_name='var', name='id', value=cls.generate_random_str()))
        root.append(cls.generate_tag(tag_name='var', name='level', value=str(randint(1, 100))))
        objects = cls.generate_tag(tag_name='objects')
        for _ in range(randint(1, 10)):
            objects.append(cls.generate_tag(tag_name='object',
                                            name=cls.generate_random_str(reservation_id=list(root)[0].attrib['value'])))
        root.append(objects)
        return root

    @classmethod
    def generate_all_xml(cls, count_files: int = 100):
        """
        Функция для создания конкретного числа xml документов
        :param count_files:
        """

        if count_files <= 0:
            assert KeyError('Dont right set count xml files! Must be >= 1')

        for index_file in range(1, count_files + 1):
            with open(path.join(DIR_XML_FILES, f'file_{index_file}.xml'), mode='w', encoding='utf-8') as file:
                file.write(
                    ElementTree.tostring(
                        cls.generate_one_xml(), method="xml"
                    ).decode(encoding='utf-8')
                )

    @classmethod
    def get_value_by_name_tag_in_xml(cls, xml_str: str):
        """
        Функция для извлечения данных из xml документа
        :param xml_str: строка, содержимое которой - xml
        """

        xml = ElementTree.fromstring(xml_str)
        id_ = None
        level = None
        list_object_names = []

        for tag in list(xml):
            if tag.tag == 'var':
                if tag.attrib.get('name', None) and tag.attrib.get('name', '') == 'id':
                    id_ = tag.attrib.get('value', '')
                    continue
                if tag.attrib.get('name', None) and tag.attrib.get('name', '') == 'level':
                    level = tag.attrib.get('value', '')
                    continue

            if tag.tag == 'objects':
                for tag_object in list(tag):
                    if tag_object.tag == 'object':
                        list_object_names.append(tag_object.attrib.get('name', ''))

        return id_, level, list_object_names





