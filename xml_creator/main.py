from xml_creator.file_util import *
import zipfile
"""
Написать программу на Python, которая делает следующие действия: 

1. Создает 50 zip-архивов, в каждом 100 xml файлов со случайными данными следующей структуры: 

<root> 
<var name=’id’ value=’<случайное уникальное строковое значение>’/> 
<var name=’level’ value=’<случайное число от 1 до 100>’/> 
<objects> 
<object name=’<случайное строковое значение>’/> 
<object name=’<случайное строковое значение>’/> 
… 
</objects> 
</root> 

В тэге objects случайное число (от 1 до 10) вложенных тэгов object. 

2. Обрабатывает директорию с полученными zip архивами, разбирает вложенные xml файлы и формирует 2 csv файла: 
Первый: id, level - по одной строке на каждый xml файл 
Второй: id, object_name - по отдельной строке для каждого тэга object (получится от 1 до 10 строк на каждый xml файл) 

Очень желательно сделать так, чтобы задание 2 эффективно использовало ресурсы многоядерного процессора.  
Также желательно чтобы программа работала быстро. 
"""


def run_first_task(count_files: int = 50):
    """
    Функция для создания заданного числа архивов с xml документами
    :param count_files: количество zip архивов
    """

    if count_files <= 0:
        assert KeyError('Dont right set count zip files! Must be >= 1')

    for index_file in range(1, count_files + 1):
        zip_ = zipfile.ZipFile(path.join(DIR_ARCH_FILES, f'zip_{index_file}.zip'), 'w')
        GenerateFile.generate_all_xml()
        for root, _, files in walk(DIR_XML_FILES):
            for file in files:
                zip_.write(path.join(root, file), file)
        zip_.close()


def run_second_task():
    """Функция для получения данных из xml файлов в zip архивах"""

    with open(path.join(DIR_CSV_FILES, '1.csv'), mode='w') as csv_1,\
            open(path.join(DIR_CSV_FILES, '2.csv'), mode='w') as csv_2:
        for root, _, files in walk(DIR_ARCH_FILES):
            for file in files:
                zip_ = zipfile.ZipFile(path.join(root, file))
                for file_in_arch in zip_.namelist():
                    xml_file = zip_.read(file_in_arch).decode(encoding='utf-8')
                    id_, level, list_object_names = GenerateFile.get_value_by_name_tag_in_xml(xml_str=xml_file)
                    csv_1.write(f'{id_}, {level}\n')
                    for object_name in list_object_names:
                        csv_2.write(f'{id_}, {object_name}\n')


if __name__ == "__main__":
    try:
        run_first_task()
        run_second_task()
    except KeyError as err:
        print(repr(err))

