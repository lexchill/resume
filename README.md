# README #

### Маленький репозиторий для демонстрации скилов одного разработчика ###


### Проекты данного репозитория ###

* articles_storage
* file_parser
* Instagram
* VK
* XML Creator
* Telepizza_bot

### articlestorage ###
Часть проекта с микросервисной архитектурой для сохранения контента публичных статей из сети интернет.
Севрис авторизации на JWT с асинхронным шифрованием + Redis для сессий. С возможностью выбора БД для хранения пользователей.
Сервис для получения контента статей с возможностью создавать свои шаблоны под каждый сайт.
Сервис полнотекстового поиска по заголовкам статей и по их содержимому на основе MongoDB.

### file_parser ###
Тестовое задание от команды по сбору данных из Kaspersky

### Instagram ###
Часть полноценного проекта для сбора данных из инстаграм (посты, комментарии, подписки)

### VK ###
Часть полноценного проекта для сбора данных (разными способами) из ВК 

### XML Creator ###
Скрипт для работы с xml и zip файлами

### Telepizza ###
Телеграм бот для создания нового аккаунта в Telepizza для использования 50% скидки при заказе