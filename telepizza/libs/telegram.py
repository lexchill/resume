from aiogram import Bot, Dispatcher
from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove
from aiogram.types import Message

from telepizza_bot.configs.enums import KeyWords, Commands
from telepizza_bot.models.config import Config
from telepizza_bot.libs.viewer import View

config = Config.get_class_config_bot()


class Telegram:

    bot = Bot(token=config.bot_token)
    dp = Dispatcher(bot)

    keyboard_get_account = View.create_reply_keyboard().add(
        View.create_simple_button(text=Commands.get_account.value)
    )

    keyboard_send_fio = View.create_reply_keyboard().add(
        View.create_simple_button(text=Commands.generate_fio.value, request_contact=True),
        View.create_simple_button(text=Commands.input_fio.value)
    )

    keyboard_send_phone = View.create_reply_keyboard().add(
        View.create_simple_button(text=Commands.self_phone.value, request_contact=True),
        View.create_simple_button(text=Commands.input_phone.value)
    )

    keyboard_banks = View.create_inline_keyboard().add(
        View.create_inline_url_button(text='Сбер', url='https://www.sberbank.ru/ru/person/remittance/easy_transfers')
    ).add(View.create_inline_url_button(text='ВК', url='https://www.vk.com/'))

    @classmethod
    async def send_message(cls, tg_id: int, **kwargs):
        await cls.bot.send_message(tg_id, **kwargs)
        #await cls.bot.session.close()

    @classmethod
    async def reply_message(cls, message: Message, **kwargs):
        await message.reply(**kwargs)

    @staticmethod
    def get_from_user(message: Message) -> dict:
        if message.from_user:
            return {
                KeyWords.first_name.name: message.from_user.first_name,
                KeyWords.last_name.name: message.from_user.last_name,
                KeyWords.username.name: message.from_user.username,
                KeyWords.tg_id.name: message.from_user.id
            }


