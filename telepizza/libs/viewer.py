from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton

class View:

    @classmethod
    def create_reply_keyboard(cls, **kwargs):
        return ReplyKeyboardMarkup(resize_keyboard=True, **kwargs)

    @classmethod
    def create_inline_keyboard(cls, **kwargs):
        return InlineKeyboardMarkup(**kwargs)

    @classmethod
    def create_simple_button(cls, text: str, **kwargs):
        return KeyboardButton(text, **kwargs)

    @classmethod
    def create_inline_url_button(cls, text: str, url: str, **kwargs):
        return InlineKeyboardButton(text=text, url=url, **kwargs)

    @classmethod
    def create_inline_call_back_button(cls, text: str, call_back: str, **kwargs):
        return InlineKeyboardButton(text=text, call_back=call_back, **kwargs)