from user_agent import generate_user_agent
import requests
import json


def get_proxy():
    return {}


class Request:

    __session = requests.Session()
    __session.headers = {'user-agent': generate_user_agent(device_type="all", os=('mac', 'linux'))}


    @classmethod
    def __request(cls, method: str, get_json: bool, **kwargs):
        kwargs['proxies'] = get_proxy()
        response = cls.__session.request(method, **kwargs)
        if not response.ok:
            return
        else:
            if get_json:
                return json.loads(response.text)
            else:
                return response.text


    @classmethod
    def request(cls, method: str, get_json: bool, **kwargs):
        return cls.__request(method=method, get_json=get_json, **kwargs)









