from telepizza_bot.configs.config import database
from peewee import chunked

class Db:

    @classmethod
    def insert_many_row(cls, model, list_data):
        with database.atomic():
            for batch in chunked(list_data, 10):
                id_new_rows = model.insert_many(batch).on_conflict_ignore().execute()
                for tuple_id in id_new_rows:
                    yield tuple_id[0]

    @classmethod
    def update(cls, model, id: int, **kwargs):
        object = model.get(model.id == id)
        for item in kwargs.items():
            setattr(object, item[0], item[1])
        object.save()













