import random
from datetime import datetime

from telepizza_bot.libs.db import Db
from telepizza_bot.models.qiwi_history_payment import QiwiHistoryPayments
from telepizza_bot.models.qiwi import QiwiToken
from telepizza_bot.models.request_payment import RequestPayment

from telepizza_bot.libs.request import Request
from telepizza_bot.configs.enums import TextBlock

def decor_for_update_session(function):
    """Декоратор для получения данных о qiwi кошельке на основе выбранного кошелька в базе"""
    def wrapper(**kwargs):
        if kwargs.get('qiwi_phone', None) is not None:
            qiwi_config = QiwiToken.get_qiwi_config(qiwi_phone=kwargs['qiwi_phone'])
        else:
            qiwi_config = QiwiToken.get_qiwi_config()

        assert qiwi_config.token is not None and qiwi_config.phone is not None, ''

        headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + qiwi_config.token
        }
        kwargs_new = {
            'headers': headers,
            'qiwi_phone': qiwi_config.phone,
            'kwargs': kwargs
        }
        return function(**kwargs_new)

    return wrapper


@decor_for_update_session
def api_get_balance_wallet(**kwargs):
    headers = kwargs['headers']
    qiwi_phone = kwargs['qiwi_phone']
    url = Qiwi.url_base + Qiwi.url_for_get_balance.format(wallet=qiwi_phone)
    answer_json = Request.request(method='GET', get_json=True, url=url, headers=headers)
    if answer_json:
        balance = int(answer_json['accounts'][0]['balance']['amount'])
        QiwiToken.update_qiwi_wallet(phone=qiwi_phone, balance=balance)
        return balance



@decor_for_update_session
def api_get_new_transaction_from_history_payment(**kwargs):
    use_qiwi_wallet_phone = kwargs['qiwi_phone']
    url = Qiwi.url_base + Qiwi.url_for_get_history_payment.format(wallet=use_qiwi_wallet_phone)
    headers = kwargs['headers']
    args = kwargs['kwargs']
    answer_json = Request.request(method='GET', get_json=True, url=url, headers=headers, params=args)
    if answer_json:
        if answer_json.get('data', None) is not None:
            list_get_txn_id = [transaction['txnId'] for transaction in answer_json['data']]
            list_exist_transaction = [transaction.txnId for transaction in
                                      QiwiHistoryPayments.get_list_transaction_by_txn_id(list_txn_id_transaction=list_get_txn_id)]

            list_new_transaction = [Qiwi.convert_tran_qiwi_service_to_tran_qiwi_self_table(transaction=transaction)
                                    for transaction in answer_json['data']
                                    if transaction['txnId'] not in list_exist_transaction]

            list_id_new_transaction = [transaction for transaction in
                                       Db.insert_many_row(model=QiwiHistoryPayments,
                                                          list_data=list_new_transaction)]
            return list_id_new_transaction
        else:
            raise AssertionError('Qiwi return answer without field "data"')
    else:
        raise AssertionError('Qiwi does not return answer')


class Qiwi:

    url_base: str = 'https://edge.qiwi.com'
    url_for_get_history_payment: str = '/payment-history/v2/persons/{wallet}/payments'
    url_for_get_balance: str = '/funding-sources/v2/persons/{wallet}/accounts'


    @staticmethod
    def get_active_qiwi_phone():
        qiwi_config = QiwiToken.get_qiwi_config()
        return qiwi_config.phone

    @staticmethod
    def get_active_qiwi_card():
        qiwi_config = QiwiToken.get_qiwi_config()
        return qiwi_config.card

    @staticmethod
    def get_active_qiwi_token():
        qiwi_config = QiwiToken.get_qiwi_config()
        return qiwi_config.token


    @classmethod
    def convert_tran_qiwi_service_to_tran_qiwi_self_table(cls, transaction: dict) -> dict:
        dict_transaction_for_table = {key: value for key, value in transaction.items() if
                                      key in QiwiHistoryPayments.get_fields_class()}
        if transaction.get('sum', None) is not None:
            dict_transaction_for_table['total_summa'] = int(transaction['sum'].get('amount', 0))
            if len(str(transaction['sum'].get('amount', 0)).split('.')) > 1:
                dict_transaction_for_table['penny'] = int(str(transaction['sum'].get('amount', 0)).split('.')[1])
            else:
                dict_transaction_for_table['penny'] = 0
            dict_transaction_for_table['currency'] = transaction['sum'].get('currency', '')

            return dict_transaction_for_table

    @classmethod
    def get_important_info_from_transaction(cls, transaction: QiwiHistoryPayments):
        return {
            'id': transaction.id,
            'date': transaction.date,
            'total_summa': transaction.total_summa,
            'penny': transaction.penny,
            'currency': transaction.currency
        }


def func_generate_all_list_penny():
    return [value for value in range(11, 100)
            if value not in [20, 30, 40, 50, 60, 70, 80, 90]]


class Paymnet:

    list_penny_for_payment = func_generate_all_list_penny()

    @classmethod
    def generate_penny(cls, tg_id: int):
        index_penny = random.randint(0, len(cls.list_penny_for_payment) - 1)
        penny_for_set = cls.list_penny_for_payment[index_penny]

        exist_penny = RequestPayment.get_exist_penny_for_payment(tg_id=tg_id)

        if exist_penny:
            return exist_penny, False
        else:
            del cls.list_penny_for_payment[index_penny]
            return penny_for_set, True


    @classmethod
    def update_list_penny_for_payment(cls):
        cls.list_penny_for_payment = func_generate_all_list_penny()


    @classmethod
    def get_message_for_send_user(cls, tg_id: int, order_id: int):
        if not cls.list_penny_for_payment:
            cls.update_list_penny_for_payment()

        penny, new_payment = cls.generate_penny(
            tg_id=tg_id
        )

        if new_payment:
            RequestPayment.insert_request_payment(
                id_tg=tg_id,
                penny=penny,
                id_order=order_id,
                request_date=datetime.now()
            )
            return TextBlock.new_need_pay.value.format(penny)
        else:
            return TextBlock.old_need_pay.value.format(penny)
