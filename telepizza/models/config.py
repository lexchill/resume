from peewee import Model, TextField, IntegerField
from telepizza_bot.configs.config import database

class Config(Model):
    id = IntegerField()
    key = TextField()
    value = TextField()
    description = TextField()

    class Meta:
        db_table = 'config'
        database = database
        schema = 'public'


    @property
    def get_name_class(self):
        return self.__class__.__name__


    @classmethod
    def get_class_config_bot(cls):
        return type('Config', (),
                    {
                        conf.key: conf.value
                        for conf in [config for config in cls.select()]
                    }
        )
