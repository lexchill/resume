from peewee import Model, BigIntegerField, TextField, IntegerField, BooleanField, DoesNotExist
from telepizza_bot.configs.config import database
from playhouse.postgres_ext import DateTimeTZField


class QiwiToken(Model):
    id = BigIntegerField()
    phone = TextField()
    card = TextField()
    token = TextField()
    date_death = DateTimeTZField()
    balance = IntegerField()
    is_use_now = BooleanField()

    class Meta:
        db_table = 'qiwi_token'
        database = database
        schema = 'public'


    @property
    def get_name_class(self):
        return self.__class__.__name__

    @classmethod
    def get_empty_qiwi_wallet(cls):
        try:
            qiwi_config = cls.select().order_by(cls.balance).get()
            return qiwi_config
        except DoesNotExist:
            raise AssertionError('Qiwi config for use not found!')

    @classmethod
    def update_qiwi_wallet(cls, phone: str, **kwargs):
        try:
            qiwi_wallet = cls.get(cls.phone == phone)
            for item in kwargs.items():
                setattr(qiwi_wallet, item[0], item[1])
            qiwi_wallet.save()

        except DoesNotExist:
            raise AssertionError('Qiwi wallet with phone = {} not exists'.format(phone))

    @classmethod
    def get_qiwi_config(cls, qiwi_phone: str = None):
        try:
            if qiwi_phone:
                qiwi_config = cls.get(cls.phone == qiwi_phone)
            else:
                qiwi_config = cls.get(cls.is_use_now == True)

            return qiwi_config

        except DoesNotExist:
            raise AssertionError('Qiwi config for use not found!')






