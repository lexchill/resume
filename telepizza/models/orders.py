from peewee import Model, BigIntegerField, TextField, IntegerField, DoesNotExist
from telepizza_bot.configs.config import database
from datetime import datetime

class Orders(Model):
    id = IntegerField()
    id_tg = BigIntegerField()
    first_name = TextField()
    last_name = TextField()
    phone_call_back = TextField()
    login = TextField()
    passwd = TextField()


    class Meta:
        db_table = 'orders'
        database = database
        schema = 'public'

    def get_list_value_fields(self):
        return [
            self.id,
            self.id_tg,
            self.first_name,
            self.last_name,
            self.phone_call_back,
            # self.login,
            # self.passwd
        ]

    @classmethod
    def get_list_fields_model(cls) -> list:
        return [
            cls.id,
            cls.id_tg,
            cls.first_name,
            cls.last_name,
            cls.phone_call_back,
            cls.login,
            cls.passwd
        ]

    @classmethod
    def insert_order(cls, **kwargs):
        return cls.insert(**kwargs).execute()

    @classmethod
    def check_exist_order_by_tg_id(cls, tg_id: int) -> bool:
        try:
            id_order = Orders.get(Orders.id_tg==tg_id)
            return True
        except DoesNotExist:
            return False

    @classmethod
    def update_order_by_id(cls, id: int, **kwargs):
        order = cls.get(cls.id == id)
        order = cls.select().where(cls.id == id)
        for item in kwargs.items():
            setattr(order, item[0], item[1])
        order.save()


    @classmethod
    def get_last_order_user_by_tg_id(cls, tg_id: int):
        list_orders = list(Orders.select().where(Orders.id_tg == tg_id).execute())
        if list_orders:
            dict_id_order_count_null_fields = {
                order.id: order.get_list_value_fields().count(None) for order in list_orders
            }
            tuple_id_order_count_null_field = sorted(dict_id_order_count_null_fields.items(), key=lambda order: order[1])[-1]
            if tuple_id_order_count_null_field[1] == 0:
                return None
            else:
                return tuple_id_order_count_null_field
        else:
            return None









