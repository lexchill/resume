import functools
import json
from peewee import Model, BigIntegerField, TextField, IntegerField, DoesNotExist
from playhouse.postgres_ext import DateTimeTZField
from playhouse.sqlite_ext import JSONField
from telepizza_bot.configs.config import database
from datetime import datetime, timedelta
from telepizza_bot.configs.enums import KeyWords


class RequestPayment(Model):
    id = BigIntegerField()
    id_tg = BigIntegerField()
    id_transaction = BigIntegerField()
    id_order = IntegerField()
    penny = IntegerField()
    request_date = DateTimeTZField()


    class Meta:
        db_table = 'request_payment'
        database = database
        schema = 'public'


    @property
    def get_name_class(self):
        return self.__class__.__name__

    @classmethod
    def insert_request_payment(cls, **kwargs):
        kwargs['request_date'] = datetime.now()
        RequestPayment.insert(**kwargs).on_conflict_ignore().execute()

    @classmethod
    def get_will_payment_by_penny_datetime(cls, penny: int, date: datetime):
        try:
            limit_time = date - timedelta(minutes=15)
            request_payment = cls.get((cls.id_transaction == None) &
                                      (cls.penny == penny) &
                                      (cls.request_date >= limit_time))
            return request_payment

        except DoesNotExist:
            return None

    @classmethod
    def check_exist_dont_pay_request(cls, tg_id: int, count_minute=None):
        try:
            if count_minute:
                limit_time = datetime.now() - timedelta(minutes=count_minute)
                request_payment = cls.get(
                    (cls.id_tg == tg_id) &
                    (cls.id_transaction == None) &
                    (cls.request_date >= limit_time)
                )

                return request_payment

            else:
                request_payment = cls.get(
                    (cls.id_tg == tg_id) &
                    (cls.id_transaction == None)
                )

                return request_payment

        except DoesNotExist:
            return None

    @classmethod
    def get_exist_penny_for_payment(cls, tg_id: int):
        try:
            limit_time = datetime.now() - timedelta(minutes=15)
            request_payment = cls.get(
                (cls.id_tg == tg_id) &
                (cls.id_transaction == None) &
                (cls.request_date >= limit_time)
            )
            return request_payment.penny

        except DoesNotExist:
            return None

    # @classmethod
    # def set_penny_for_payment(cls, tg_id: int, id_order: int, penny_for_set: int):
    #         request_payment_id = cls.insert(
    #             id_tg=tg_id,
    #             penny=penny_for_set,
    #             id_order=id_order,
    #             request_date=datetime.now()
    #         ).execute()
    #
    #         if request_payment_id is None:
    #             raise ValueError(
    #                 'Operation INSERT new request_payment return None! Check the connection to DB')



