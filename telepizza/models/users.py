from peewee import Model, BigIntegerField, TextField, IntegerField
from playhouse.postgres_ext import DateTimeTZField
from telepizza_bot.configs.config import database
from datetime import datetime

class Users(Model):
    id = IntegerField()
    tg_id = BigIntegerField()
    first_name = TextField()
    last_name = TextField()
    username = TextField()
    connected_date = DateTimeTZField()


    class Meta:
        db_table = 'users'
        database = database
        schema = 'public'

    @classmethod
    def insert_user(cls, **kwargs):
        kwargs['connected_date'] = datetime.now()
        return cls.insert(**kwargs).on_conflict_ignore().execute()




