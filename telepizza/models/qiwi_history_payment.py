from peewee import Model, BigIntegerField, TextField, IntegerField
from telepizza_bot.configs.config import database
from playhouse.postgres_ext import DateTimeTZField

class QiwiHistoryPayments(Model):
    id = BigIntegerField()
    txnId = BigIntegerField()
    personId = BigIntegerField()
    date = DateTimeTZField()
    errorCode = IntegerField()
    error = TextField()
    type = TextField()
    status = TextField()
    statusText = TextField()
    trmTxnId = BigIntegerField()
    account = TextField()
    total_summa = IntegerField()
    penny = IntegerField()
    currency = TextField()

    class Meta:
        db_table = 'qiwi_history_payment'
        database = database
        schema = 'public'

    @classmethod
    def get_fields_class(cls):
        return [name_field for name_field in cls.__dict__.keys() if name_field[0] != '_']


    @property
    def get_name_class(self):
        return self.__class__.__name__

    @classmethod
    def get_list_transaction_by_txn_id(cls, list_txn_id_transaction: list):
        for transaction in cls.select().where(cls.txnId << list_txn_id_transaction):
            yield transaction

    @classmethod
    def get_list_transaction_by_id(cls, list_id_transaction: list):
        for transaction in cls.select().where(cls.id << list_id_transaction):
            yield transaction