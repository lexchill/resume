from telepizza_bot.libs.qiwi import Qiwi, api_get_balance_wallet, api_get_new_transaction_from_history_payment

from telepizza_bot.models.request_payment import RequestPayment
from telepizza_bot.models.qiwi_history_payment import QiwiHistoryPayments
from telepizza_bot.models.qiwi import QiwiToken

from telepizza_bot.models.config import Config
from telepizza_bot.libs.db import Db
import time
import asyncio

config = Config.get_class_config_bot()

async def run_qiwi_cheker():
    while True:
        try:
            list_id_new_transaction = api_get_new_transaction_from_history_payment(rows=30)
            if list_id_new_transaction:
                list_transactions_with_important_info = [Qiwi.get_important_info_from_transaction(transaction)
                                                         for transaction in QiwiHistoryPayments.get_list_transaction_by_id(list_id_new_transaction)]

                for payment_info in list_transactions_with_important_info:

                    live_request_payment = RequestPayment.get_will_payment_by_penny_datetime(
                        penny=payment_info['penny'],
                        date=payment_info['date']
                    )

                    if live_request_payment:

                        Db.update(model=RequestPayment,
                                  id=live_request_payment.id,
                                  id_transaction=payment_info['id'])

                        if payment_info['total_summa'] == int(config.cost_pay):
                            tg_id = live_request_payment.id_tg
                        else:
                            pass
                    else:
                        a = f'Не найдена строка в таблицу WillPayment\nПревышет лимит 15 минут или нет нужных копеек!'

        except Exception as err:
            print(repr(err))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_qiwi_cheker())