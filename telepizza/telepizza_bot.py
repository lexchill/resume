from aiogram import executor
from aiogram.types import ReplyKeyboardRemove
from telepizza_bot.libs.telegram import Telegram
import logging
import re
from datetime import datetime

from telepizza_bot.configs.enums import Commands, TextBlock
from telepizza_bot.models.users import Users
from telepizza_bot.models.orders import Orders
from telepizza_bot.models.request_payment import RequestPayment
from telepizza_bot.libs.qiwi import Paymnet
from telepizza_bot.libs.db import Db



regex_phone = re.compile(r'^79\d{9}$')
regex_fio = re.compile(r'^\s*[a-zа-яёЁЇїІіЄєҐґ]{2,20}\s+[a-zа-яёЁЇїІіЄєҐґ]{2,20}\s*$', flags=re.IGNORECASE)

def exist_dont_pay_request(tg_id: int):
    dont_pay_order = RequestPayment.check_exist_dont_pay_request(tg_id=tg_id, count_minute=15)
    if dont_pay_order:
        return dont_pay_order.penny
    else:
        old_dont_pay_order = RequestPayment.check_exist_dont_pay_request(tg_id=tg_id)
        if old_dont_pay_order:
            new_penny, _ = Paymnet.generate_penny(tg_id=tg_id)
            Db.update(model=RequestPayment, id=old_dont_pay_order.id,
                      penny=new_penny, request_date=datetime.now())
            return new_penny
        else:
            return None
"""
если есть неоплаченный счет в течении 15 минут, то выдаем его же
если есть просто просроченный счет, его удаляем и заводим новый
"""

def processing_get_account(tg_id:int):
    tuple_id_order_count_null_field = Orders.get_last_order_user_by_tg_id(tg_id=tg_id)
    if tuple_id_order_count_null_field is None:
        Orders.insert_order(id_tg=tg_id)

async def processing_get_phone(tg_id:int, phone: str):
    tuple_id_order_count_null_field = Orders.get_last_order_user_by_tg_id(tg_id=tg_id)

    if tuple_id_order_count_null_field is None or tuple_id_order_count_null_field[1] > 1:
        await Telegram.send_message(tg_id=tg_id,
                                    text=TextBlock.get_fio_after_get_phone.value,
                                    reply_markup=Telegram.keyboard_send_fio)
    else:
        Orders.update_order_by_id(id=tuple_id_order_count_null_field[0], phone_call_back=phone)

        answer_message = Paymnet.get_message_for_send_user(tg_id=tg_id, order_id=tuple_id_order_count_null_field[0])

        if answer_message:
            await Telegram.send_message(tg_id=tg_id,
                                        text=answer_message,
                                        reply_markup=Telegram.keyboard_get_account)
        else:
            await Telegram.send_message(tg_id=tg_id,
                                        text=TextBlock.error.value,
                                        reply_markup=ReplyKeyboardRemove())



def processing_get_fio(tg_id:int, fio: str):
    tuple_id_order_count_null_field = Orders.get_last_order_user_by_tg_id(tg_id=tg_id)
    if tuple_id_order_count_null_field is None:
        id_order = Orders.insert_order(id_tg=tg_id)
    else:
        id_order = tuple_id_order_count_null_field[0]
    first_name, last_name = fio.split(' ')
    Orders.update_order_by_id(id=id_order, first_name=first_name, last_name=last_name)


@Telegram.dp.message_handler(commands=['help', 'start'])
async def processing_command(message):
    try:
        if message.text == Commands.start.value:
            user = Telegram.get_from_user(message=message)
            Users.insert_user(**user)
            await Telegram.send_message(tg_id=message.from_user.id,
                                        text=TextBlock.start.value,
                                        reply_markup=Telegram.keyboard_get_account)

        elif message.text == Commands.help.value:
            await Telegram.send_message(tg_id=message.from_user.id,
                                        text=TextBlock.help.value,
                                        reply_markup=Telegram.keyboard_get_account)
    except Exception as err:
        print(repr(err))
        await Telegram.reply_message(message=message,
                                     text=TextBlock.error.value,
                                     reply_markup=ReplyKeyboardRemove())



@Telegram.dp.message_handler(content_types=['contact'])
async def processing_contact(message):
    try:
        penny_in_dont_pay_score = exist_dont_pay_request(tg_id=message.from_user.id)
        if penny_in_dont_pay_score:
            await Telegram.reply_message(message=message,
                                         text=TextBlock.old_need_pay.value.format(penny_in_dont_pay_score),
                                         reply_markup=Telegram.keyboard_get_account)
            return

        if message.values.get('contact', None):
            if message.values['contact'].phone_number:
                await processing_get_phone(tg_id=message.from_user.id,
                                           phone=message.values['contact'].phone_number)
            else:
                await Telegram.reply_message(message=message,
                                             text=TextBlock.not_found_phone.value,
                                             reply_markup=ReplyKeyboardRemove())

    except Exception as err:
        print(repr(err))
        await Telegram.reply_message(message=message,
                                     text=TextBlock.error.value,
                                     reply_markup=ReplyKeyboardRemove())


@Telegram.dp.message_handler(content_types=['text'])
async def processing_text(message):
    try:
        penny_in_dont_pay_score = exist_dont_pay_request(tg_id=message.from_user.id)
        if penny_in_dont_pay_score:
            await Telegram.reply_message(message=message,
                                         text=TextBlock.old_need_pay.value.format(penny_in_dont_pay_score),
                                         reply_markup=Telegram.keyboard_get_account)
            return


        if message.text == Commands.get_account.value:
            processing_get_account(tg_id=message.from_user.id)
            await Telegram.reply_message(message=message,
                                         text=TextBlock.get_fio.value,
                                         reply_markup=Telegram.keyboard_send_fio)

        elif message.text == Commands.input_phone.value:
            await Telegram.reply_message(message=message,
                                         text=TextBlock.input_phone.value,
                                         reply_markup=ReplyKeyboardRemove())

        elif message.text == Commands.input_fio.value:
            await Telegram.reply_message(message=message,
                                         text=TextBlock.input_fio.value,
                                         reply_markup=ReplyKeyboardRemove())

        elif message.text == Commands.generate_fio.value:
            pass


        elif regex_phone.match(message.text) is not None:
            await processing_get_phone(tg_id=message.from_user.id,
                                       phone=message.text)

        elif regex_fio.match(message.text) is not None:
            processing_get_fio(tg_id=message.from_user.id,
                               fio=message.text)
            await Telegram.reply_message(message=message,
                                         text=TextBlock.get_phone.value,
                                         reply_markup=Telegram.keyboard_send_phone)

        else:
            await Telegram.reply_message(message=message,
                                         text=TextBlock.unknown_text.value,
                                         reply_markup=Telegram.keyboard_send_fio)
        # else:
        #     await Telegram.reply_message(message=message,
        #                                  text=TextBlock.old_need_pay.value.format(penny_in_dont_pay_score),
        #                                  reply_markup=Telegram.keyboard_get_account)

    except Exception as err:
        print(repr(err))
        await message.reply(text=TextBlock.error.value, reply_markup=ReplyKeyboardRemove())


if __name__ == "__main__":
    while True:
        try:
            executor.start_polling(Telegram.dp, skip_updates=False)
        except Exception as err:
            print(repr(err))





