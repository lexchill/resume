import os
import gzip
import logging
import argparse

from datetime import datetime
from collections import defaultdict
from typing import IO, Dict, Generator


parser = argparse.ArgumentParser(description='Convert file to list dict')
parser.add_argument('--path', default="example.txt", help='Path to file')


log_file_path = f'{datetime.now().strftime("%d_%m_%Y")}.log'
log_file_handler = logging.FileHandler(log_file_path)
log_formatter = logging.Formatter('%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s')
log_file_handler.setFormatter(log_formatter)
log_file_handler.setLevel(logging.INFO)

logger = logging.getLogger()
logger.addHandler(log_file_handler)


def parse_file_assist(file_descriptor: IO) -> Generator[Dict[str, str], None, None]:
    """
    Вспомогающая функция, которой не важно откуда идет чтения данных (архив, файл)
    :param file_descriptor: дескриптор файла для чтения
    :return:
    """
    document = defaultdict(str)
    text_key, text_value = '', ''

    for line in file_descriptor:
        if isinstance(line, bytes):
            line = line.decode("utf-8")

        if line == "\n" or line == "\r\n":
            # признак конца документа
            if document:
                yield document
                document.clear()
                text_key, text_value = '', ''
        else:
            if line.startswith("#"):
                # признак комментария
                continue
            elif line.startswith(" "):
                # признак продолжения значения текущего ключа
                if text_key:
                    document[text_key] += f"\n{text_value.strip()}" if text_key in document else text_value.strip()
                    continue
                else:
                    # если ключ пуст, то мы ожидаем строку, которая не будет начинаться с " "
                    logger.warning(f"Не валидная строка: \"{line}\"")
                    continue
            elif line.startswith(":"):
                # признак строки без ключа
                logger.warning(f"Не валидная строка: \"{line}\"")
                continue
            else:
                if ":" not in line:
                    logger.warning(f"Не валидная строка: \"{line}\"")
                    continue

                text_key, text_value = line.split(":", maxsplit=1)
                document[text_key] += f"\n{text_value.strip()}" if text_key in document else text_value.strip()


def parse_file(path: str) -> Generator[Dict[str, str], None, None]:
    full_name = os.path.basename(path)

    if os.path.splitext(full_name)[1] == ".gz":
        file_descriptor = gzip.open(filename=path, mode="rb")
    else:
        file_descriptor = open(file=path, mode="r", encoding="utf-8")

    try:
        for document in parse_file_assist(file_descriptor=file_descriptor):
            yield document
    except Exception as err:
        logger.error(err, exc_info=True)
    finally:
        file_descriptor.close()


def load_data(path: str):
    if os.path.exists(path) is False:
        logger.error(f"Файл: {path} не найден")
        return
    for document in parse_file(path):
        # load document to database (or do something else)
        for key, value in document.items():
            print(f"{key}: {value}")
        print("\n\n")

    logger.warning(f"Файл: {path} успешно конвертирован")


if __name__ == "__main__":
    args = parser.parse_args()
    load_data(path=args.path)
